# Syntelix MQTT Publisher

This is a simple app to solve the need of sending a boat's IMO, coordinates and timestamp to a MQTT queue in order to be
tracked by other apps.

It sends the data automatically every minute, checking if the connections is correct and storing the messages which were
not sent in a local database. The messages stored, can be retried executing a console command which will output the
result.

There is also a manual way of sending data, which can also be accessed through a command.

## Requirements

- Docker
- Docker Compose

## Installation

Clone the repository from Bitbucket or download its sources:

````
git clone https://vinopatxoko@bitbucket.org/vinopatxoko/syntelix-mqtt-publisher.git
````

Go to the project root folder and use Docker to start. The first time, it will download all the required stuff and build
the images to bring up the project. Next time it will pick these built images and run it in a hurry.

````
docker-compose up -d
````

That's all, the project should be running. Due to the command that is executed when the container starts, the
application should have started sending or storing the messages.

## Configuration

A basic configuration can be found in the `.env` file of the root folder.

````
IMO=IMO0000000

MQTT_HOST=6e437b1ad8c2452f971c8a55fc644b91.s1.eu.hivemq.cloud
MQTT_PORT=8883
MQTT_USER=publisher
MQTT_PASS=s3qFAbyT7T
MQTT_CA_PATH=/etc/ssl/certs/ca-certificates.crt
MQTT_SSL_ENABLE=1
````

The key `IMO` sets the default value for the messages generated automatically. It is not validated soo it should be real
in this point. When the manual command for sending data is run, you will be prompted to enter a different value, and in
this case it will be validated.

All the keys starting with `MQTT` refer to the connection to the MQTT broker. The `MQTT_CA_PATH` key indicates the path
for the certificates to check with the broker and `MQTT_SSL_ENABLE` is a switch between 0 or 1 and tells the app if its
necessary to use the certificate or not.

## General operations

The console commands need to be run on the PHP container. To avoid accessing the container, it can be used `docker exec`
from the host machine. To get it even easier, it is very handy to get the container name from here:

````
docker ps

CONTAINER ID   IMAGE                COMMAND                  CREATED         STATUS         PORTS      NAMES
281cd9529346   mqtt-publisher-php   "docker-php-entrypoi…"   3 minutes ago   Up 3 minutes   9000/tcp   syntelix-mqtt-publisher_php_1
````

It should be `syntelix-mqtt-publisher_php_1` by default, but changing the configuration or running more containers at
the same time can change it. The following examples are using this name.

### Automatic message publishing

This command takes the IMO from the configuration file and generates random coordinates to send them to the MQTT broker.
If the connection fails the message will be stored in a local database. Even if a cron job is already executing this
command, it can be run on demand.

````
docker exec -it syntelix-mqtt-publisher_php_1 php bin/console syntelix:publish --auto
````

### Manual message publishing

This command asks you for the IMO, latitude and longitude. It validates the inputs and sends them to the MQTT broker. If
the connection fails the message will be stored in a local database.

````
docker exec -it syntelix-mqtt-publisher_php_1 php bin/console syntelix:publish
````

### Pending messages retry

This command tries to resend the failed messages to the MQTT broker. If succeeds, the messages are deleted, otherwise
they get stored for the next retry.

````
docker exec -it syntelix-mqtt-publisher_php_1 php bin/console syntelix:publish:pending
````

### Tests

There are some tests of the application that you can run with this:

````
docker exec -it syntelix-mqtt-publisher_php_1 php bin/phpunit
````
