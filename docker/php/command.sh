#!/bin/bash

composer install --working-dir={VAR_PROJECT_PATH}
chmod -R 777 {VAR_PROJECT_PATH}/var
chmod -R 777 {VAR_PROJECT_PATH}/bin

php {VAR_PROJECT_PATH}/bin/console doctrine:migrations:migrate --no-interaction

cron
crontab /home/cron

php-fpm