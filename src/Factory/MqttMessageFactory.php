<?php
namespace App\Factory;

use App\Entity\MqttMessage;
use App\Repository\MqttMessageRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<MqttMessage>
 *
 * @method static MqttMessage|Proxy createOne(array $attributes = [])
 * @method static MqttMessage[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static MqttMessage|Proxy find(object|array|mixed $criteria)
 * @method static MqttMessage|Proxy findOrCreate(array $attributes)
 * @method static MqttMessage|Proxy first(string $sortedField = 'id')
 * @method static MqttMessage|Proxy last(string $sortedField = 'id')
 * @method static MqttMessage|Proxy random(array $attributes = [])
 * @method static MqttMessage|Proxy randomOrCreate(array $attributes = [])
 * @method static MqttMessage[]|Proxy[] all()
 * @method static MqttMessage[]|Proxy[] findBy(array $attributes)
 * @method static MqttMessage[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static MqttMessage[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static MqttMessageRepository|RepositoryProxy repository()
 * @method MqttMessage|Proxy create(array|callable $attributes = [])
 */
final class MqttMessageFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'topic' => self::faker()->text(),
            'payload' => 'tests',
            'qos' => 0,
            'retain' => false,
        ];
    }

    protected function initialize(): self
    {
        return $this;
    }

    protected static function getClass(): string
    {
        return MqttMessage::class;
    }
}
