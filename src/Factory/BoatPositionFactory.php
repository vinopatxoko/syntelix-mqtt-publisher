<?php
namespace App\Factory;

use App\Model\BoatPosition;
use App\Model\Coordinate;
use Faker\Provider\Address;

class BoatPositionFactory
{
    public static function create(string $imo, float $latitude, float $longitude): BoatPosition
    {
        $coordinate = new Coordinate($latitude, $longitude);
        return new BoatPosition($imo, $coordinate);
    }

    public static function createAuto(): BoatPosition
    {
        $coordinate = new Coordinate(Address::latitude(), Address::longitude());
        return new BoatPosition($_ENV['IMO'], $coordinate);
    }
}