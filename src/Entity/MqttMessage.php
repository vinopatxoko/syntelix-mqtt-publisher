<?php
namespace App\Entity;

use App\Repository\MqttMessageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MqttMessageRepository::class)
 */
class MqttMessage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;
    /**
     * @ORM\Column(type="string")
     */
    private string $topic;
    /**
     * @ORM\Column(type="string")
     */
    private string $payload;
    /**
     * @ORM\Column(type="integer")
     */
    private int $qos;
    /**
     * @ORM\Column(type="boolean")
     */
    private bool $retain;

    public function __construct(string $topic, string $payload, int $qos = 0, bool $retain = false)
    {
        $this->topic = $topic;
        $this->payload = $payload;
        $this->qos = $qos;
        $this->retain = $retain;
    }

    public function getTopic(): ?string
    {
        return $this->topic;
    }

    public function setTopic(string $topic)
    {
        $this->topic = $topic;
    }

    public function getPayload(): ?string
    {
        return $this->payload;
    }

    public function setPayload(string $payload)
    {
        $this->payload = $payload;
    }

    public function getQos(): ?int
    {
        return $this->qos;
    }

    public function setQos(int $qos)
    {
        $this->qos = $qos;
    }

    public function getRetain(): ?bool
    {
        return $this->retain;
    }

    public function setRetain(bool $retain)
    {
        $this->retain = $retain;
    }
}