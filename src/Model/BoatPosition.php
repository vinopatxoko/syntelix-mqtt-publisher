<?php
namespace App\Model;

use DateTime;

class BoatPosition
{
    private string $imo;
    private int $timestamp;
    private float $latitude;
    private float $longitude;

    public function __construct(string $imo, Coordinate $coordinate)
    {
        $this->imo = $imo;
        $this->latitude = $coordinate->getLatitude();
        $this->longitude = $coordinate->getLongitude();
        $datetime = new DateTime();
        $this->timestamp = $datetime->getTimestamp();
    }

    public function getImo(): ?string
    {
        return $this->imo;
    }

    public function getTimestamp(): ?int
    {
        return $this->timestamp;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }
}