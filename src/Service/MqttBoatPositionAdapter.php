<?php
namespace App\Service;

use App\Entity\MqttMessage;
use App\Model\BoatPosition;
use Symfony\Component\Serializer\SerializerInterface;

class MqttBoatPositionAdapter
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function convert(BoatPosition $boatPosition): MqttMessage
    {
        $topic = sprintf('boats/%s/positions', $boatPosition->getImo());
        $payload = $this->serializer->serialize($boatPosition, 'json');
        return new MqttMessage($topic, $payload, 0, false);
    }
}