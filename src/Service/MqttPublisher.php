<?php
namespace App\Service;

use App\Entity\MqttMessage;
use Exception;
use Mosquitto\Client;

class MqttPublisher
{
    /**
     * @var MqttMessage[]
     */
    private array $mqttMessages;
    private Client $client;

    public function __construct()
    {
        $this->client = new Client();
        if ($_ENV['MQTT_SSL_ENABLE']) $this->client->setTlsCertificates($_ENV['MQTT_CA_PATH']);
        $this->client->setCredentials($_ENV['MQTT_USER'], $_ENV['MQTT_PASS']);
        $this->client->onConnect([$this, 'handleOnConnect']);
    }

    /**
     * @param MqttMessage[] $mqttMessages
     */
    public function publish(array $mqttMessages): bool
    {
        $this->mqttMessages = $mqttMessages;
        return $this->start();
    }

    public function handleOnConnect(int $code)
    {
        if ($code === 0) {
            foreach ($this->mqttMessages as $mqttMessage) {
                $this->client->publish(
                    $mqttMessage->getTopic(),
                    $mqttMessage->getPayload(),
                    $mqttMessage->getQos(),
                    $mqttMessage->getRetain()
                );
            }
        }
        $this->client->disconnect();
    }

    private function start(): bool
    {
        $succeeded = true;
        try {
            $this->client->connect($_ENV['MQTT_HOST'], $_ENV['MQTT_PORT']);
            $this->client->loopForever();
        } catch (Exception $exception) {
            $succeeded = false;
        }
        return $succeeded;
    }
}