<?php
namespace App\Command;

use App\Repository\MqttMessageRepository;
use App\Service\MqttPublisher;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PublishPendingCommand extends Command
{
    protected static $defaultName = 'syntelix:publish:pending';
    protected static $defaultDescription = 'Publishes failured messages to a MQTT queue';
    private MqttPublisher $mqttPublisher;
    private EntityManagerInterface $entityManager;
    private MqttMessageRepository $mqttMessageRepository;

    public function __construct(
        MqttPublisher $mqttPublisher,
        MqttMessageRepository $mqttMessageRepository,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct();
        $this->mqttPublisher = $mqttPublisher;
        $this->mqttMessageRepository = $mqttMessageRepository;
        $this->entityManager = $entityManager;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $countPublishedMessages = 0;
        $mqttMessages = $this->mqttMessageRepository->findAll();
        if ($mqttMessages) {
            foreach ($mqttMessages as $mqttMessage) {
                if ($this->mqttPublisher->publish([$mqttMessage])) {
                    $this->entityManager->remove($mqttMessage);
                    $this->entityManager->flush();
                    $countPublishedMessages++;
                }
            }
            (count($mqttMessages) - $countPublishedMessages === 0) ?
                $io->success('Pending messages have been delivered.') :
                $io->error('There are still pending messages stored. Try to retry later.');
        } else {
            $io->info('There are no pending messages.');
        }
        return Command::SUCCESS;
    }
}
