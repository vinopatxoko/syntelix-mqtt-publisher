<?php
namespace App\Command;

use App\Factory\BoatPositionFactory;
use App\Model\BoatPosition;
use App\Service\MqttBoatPositionAdapter;
use App\Service\MqttPublisher;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Provider\Address;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PublishCommand extends Command
{
    protected static $defaultName = 'syntelix:publish';
    protected static $defaultDescription = 'Publishes the position of a boat to a MQTT queue';
    private MqttPublisher $mqttPublisher;
    private MqttBoatPositionAdapter $mqttBoatPositionAdapter;
    private EntityManagerInterface $entityManager;

    public function __construct(
        MqttPublisher $mqttPublisher,
        MqttBoatPositionAdapter $mqttBoatPositionAdapter,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct();
        $this->mqttPublisher = $mqttPublisher;
        $this->mqttBoatPositionAdapter = $mqttBoatPositionAdapter;
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this->addOption('auto', null, InputOption::VALUE_NONE, 'Get the require information from the boat`s sensors');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $boatPosition = $input->getOption('auto') ?
            BoatPositionFactory::createAuto() :
            $this->askForBoatPosition($io);
        $mqttBoatPosition = $this->mqttBoatPositionAdapter->convert($boatPosition);
        if ($this->mqttPublisher->publish([$mqttBoatPosition])) {
            $io->success('The message has been delivered.');
        } else {
            $this->entityManager->persist($mqttBoatPosition);
            $this->entityManager->flush();
            $io->error('The message has not been delivered. Run `syntelix:publish:pending` command to retry all pending messages.');
        }
        return Command::SUCCESS;
    }

    private function askForBoatPosition(SymfonyStyle $io): BoatPosition
    {
        do {
            $imo = $io->ask('IMO', $_ENV['IMO']);
        } while (!$this->imoValidator($imo, $io));
        do {
            $latitude = $io->ask('Latitude', Address::latitude());
        } while (!$this->latitudeValidator($latitude, $io));
        do {
            $longitude = $io->ask('Longitude', Address::longitude());
        } while (!$this->longitudeValidator($longitude, $io));
        return BoatPositionFactory::create($imo, (float)$latitude, (float)$longitude);
    }

    /**
     * Validation of IMO strings based on the Wikipedia's information
     * @link https://en.wikipedia.org/wiki/IMO_number
     */
    private function imoValidator(string $imo, SymfonyStyle $io): bool
    {
        $isValid = false;
        if (
            strlen($imo) === 10 &&
            substr($imo, 0, 3) === 'IMO' &&
            is_numeric(substr($imo, 3))
        ) {
            $numbers = substr($imo, 3, 6);
            $securityCheck = substr($imo, 9);
            $multiplier = 7;
            $sum = 0;
            foreach (str_split($numbers) as $number) {
                $sum += $number * $multiplier;
                $multiplier--;
            }
            if ($securityCheck === substr($sum, -1, 1)) $isValid = true;
        }
        if (!$isValid) $io->error('Be sure to type a valid IMO string.');
        return $isValid;
    }

    private function latitudeValidator(string $latitude, SymfonyStyle $io): bool
    {
        $isValid = (is_numeric($latitude) && $latitude >= -90 && $latitude <= 90);
        if (!$isValid) $io->error('Be sure to type a valid float number between -90.0 and 90.0.');
        return $isValid;
    }

    private function longitudeValidator(string $longitude, SymfonyStyle $io): bool
    {
        $isValid = (is_numeric($longitude) && $longitude >= -180 && $longitude <= 180);
        if (!$isValid) $io->error('Be sure to type a valid float number between -180.0 and 180.0.');
        return $isValid;
    }
}