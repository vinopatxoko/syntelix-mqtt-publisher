<?php
namespace App\Repository;

use App\Entity\MqttMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MqttMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method MqttMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method MqttMessage[]    findAll()
 * @method MqttMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MqttMessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MqttMessage::class);
    }
}
