<?php
namespace App\Tests\Command;

use App\Service\MqttPublisher;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class PublishCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('syntelix:publish');
        $this->testAutoPublishing($command);
        $this->testManualPublishing($command);
    }

    private function testAutoPublishing(Command $command)
    {
        $commandTester = new CommandTester($command);
        $commandTester->execute([], ['auto']);
        $this->validInputDisplay($commandTester->getDisplay());
    }

    private function testManualPublishing(Command $command)
    {
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(['IMO0000013', 'IMO0000012', 'xyz', '100.0', '0.0', 'xyz', '200.0', '0.0']);
        $commandTester->execute([]);
        $this->notValidInputDisplay($commandTester->getDisplay());
        $this->validInputDisplay($commandTester->getDisplay());
    }

    private function validInputDisplay(string $display)
    {
        $mqttPublisher = new MqttPublisher();
        ($mqttPublisher->publish([])) ?
            $this->assertStringContainsString('[OK]', $display) :
            $this->assertStringContainsString('[ERROR]', $display);
    }

    private function notValidInputDisplay(string $display)
    {
        $mqttPublisher = new MqttPublisher();
        $errors = ($mqttPublisher->publish([])) ? 5 : 6;
        $this->assertEquals($errors, substr_count($display, 'ERROR'));
    }
}