<?php
namespace App\Tests\Command;

use App\Factory\MqttMessageFactory;
use App\Service\MqttPublisher;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class PublishPendingCommandTest extends KernelTestCase
{
    use Factories, ResetDatabase;

    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('syntelix:publish:pending');
        $commandTester = new CommandTester($command);
        $this->testWithoutPendingMessages($commandTester);
        $this->testWithPendingMessages($commandTester);
    }

    private function testWithoutPendingMessages(CommandTester $commandTester)
    {
        $commandTester->execute([]);
        $display = $commandTester->getDisplay();
        $this->assertStringContainsString('[INFO]', $display);
    }

    private function testWithPendingMessages($commandTester)
    {
        MqttMessageFactory::createMany(3);
        $commandTester->execute([]);
        $display = $commandTester->getDisplay();
        $mqttPublisher = new MqttPublisher();
        ($mqttPublisher->publish([])) ?
            $this->assertStringContainsString('[OK]', $display) :
            $this->assertStringContainsString('[ERROR]', $display);
    }
}